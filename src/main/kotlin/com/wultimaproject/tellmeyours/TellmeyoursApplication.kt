package com.wultimaproject.tellmeyours

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class TellmeyoursApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
             SpringApplication.run(TellmeyoursApplication::class.java, *args)
        }
    }
}
