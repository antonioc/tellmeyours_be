package com.wultimaproject.tellmeyours.repositories

import com.wultimaproject.tellmeyours.models.Show
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param


interface ShowRepository : CrudRepository<Show?, Long?> {

    @Query("SELECT (s) FROM Show s ORDER BY showCount DESC")
    fun getMostVotedShowsAbsolute(): List<Show>

    @Query("SELECT (s) FROM Show s WHERE showGenre LIKE %:showGenre% ORDER BY showCount DESC")
    fun getMostVotedShowsByGenreDesc(@Param("showGenre") showGenre: String): List<Show>

    @Query("SELECT (s) FROM Show s WHERE showGenre LIKE %:showGenre% ORDER BY showCount ASC")
    fun getMostVotedShowsByGenreAsc(@Param("showGenre") showGenre: String): List<Show>

    @Query("SELECT s.showGenre FROM Show s")
    fun findAllGenres(): List<String>

    @Query("SELECT (s) FROM Show s WHERE omdbId IS :omdbId ")
    fun getShowFromDB(omdbId: String): List<Show> //this has to be List to comply with getShowsWithParameters in ShowService

    //preset
    fun findByShowNameContaining(showName: String): List<Show>


}