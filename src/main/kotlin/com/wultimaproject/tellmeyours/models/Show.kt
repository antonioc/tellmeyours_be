package com.wultimaproject.tellmeyours.models

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name = "table_show")
class Show(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(nullable = false)
        val omdbId: String = "",

        @Column(nullable = false)
        val showName: String = "",

        @Column(nullable = false)
        var showCount: Int = 0,

        @Column(nullable = false)
        var showGenre: String = "",

        @Column(nullable = false)
        var showPosterUrl: String = "",

        @Column(nullable = false)
        var showPosterUrlDown: String = "",

        @Column(nullable = false)
        var showType: String = "",

        @Column(nullable = true)
        var showStreamingPlatform: String = ""
)

