package com.wultimaproject.tellmeyours.models

import com.fasterxml.jackson.annotation.JsonProperty

data class SearchResultOmdbShowExtended(
        @JsonProperty("Search")
        val searchResults: List<OmdbShowExtended>?
)