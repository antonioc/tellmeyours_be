package com.wultimaproject.tellmeyours.models

import com.fasterxml.jackson.annotation.JsonProperty

data class OmdbShowExtended(

        @JsonProperty("Title")
        val title: String,

        @JsonProperty("imdbID")
        val imdbID: String,

        @JsonProperty("Genre")
        val genre: String?,

        @JsonProperty("Type")
        val type: String,

        @JsonProperty("Poster")
        val poster: String
)

fun OmdbShowExtended.isValid() =
        title.isNotBlank() &&
                imdbID.isNotBlank() &&
                type.isNotBlank() &&
                poster.isNotBlank()