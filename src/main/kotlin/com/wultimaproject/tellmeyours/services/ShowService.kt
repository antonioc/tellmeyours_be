package com.wultimaproject.tellmeyours.services

import com.wultimaproject.tellmeyours.models.OmdbShowExtended
import com.wultimaproject.tellmeyours.models.SearchResultOmdbShowExtended
import com.wultimaproject.tellmeyours.models.Show
import com.wultimaproject.tellmeyours.models.isValid
import com.wultimaproject.tellmeyours.repositories.ShowRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate


@Service
class ShowService
@Autowired
constructor(val showRepository: ShowRepository) {

    fun voteShow(omdbId: String, platform: String): Show {
        var showInDb = showRepository.getShowFromDB(omdbId)[0]
        if (showInDb.showGenre.isNullOrEmpty()) {
            showRepository.delete(showInDb)
            showInDb = getDetailsShowFromOmdb(omdbId)
        }
        showInDb.showStreamingPlatform = platform
        showInDb.showCount = showInDb.showCount + 1
        showRepository.save(showInDb)
        return showInDb
    }


    fun getShowWithHighestCount(): List<Show?> {
        return showRepository.getMostVotedShowsAbsolute()
    }


    fun getAllShowGenres(): List<String> {
        return showRepository.findAllGenres()
    }

    fun getShowsWithParameters(showName: String?, omdbId: String?, showGenre: String?, sortOrder: String?)
            : List<Show?> {
        if (!showName.isNullOrEmpty()) {
            return showRepository.findByShowNameContaining(showName)
        }

        if (!omdbId.isNullOrEmpty()) {
            return showRepository.getShowFromDB(omdbId)
        }

        if (!showGenre.isNullOrEmpty()) {
            return if (!sortOrder.isNullOrEmpty() && sortOrder == "ASC")
                showRepository.getMostVotedShowsByGenreAsc(showGenre)
            else {
                showRepository.getMostVotedShowsByGenreDesc(showGenre)
            }
        }

        return showRepository.findAll().toList()
    }

    fun getShowListFromOmdb(showName: String): List<Show>? {
        val uri = "https://www.omdbapi.com/?apikey=5b4e4596&s=${showName}"
//        val uri = " "
        val restTemplate = RestTemplate()
        val result = restTemplate.getForObject(uri, SearchResultOmdbShowExtended::class.java)

        val seriesListFromOMDB = result?.searchResults?.let { searchResult ->
            val filteredShowList: MutableList<Show> = mutableListOf()
            searchResult.filter { it.type=="series" }.forEach { show ->
                if (show.isValid()) {
                    val translatedShow = Show(
                            0,
                            showName = show.title.capitalize(),
                            omdbId = show.imdbID,
                            showPosterUrl = show.poster,
                            showPosterUrlDown = scaleDown(show.poster),
                            showType = show.type
                    )
                    filteredShowList.add(translatedShow)

                    val internalShow = showRepository.getShowFromDB(show.imdbID)
                    if (internalShow.isNullOrEmpty()) {
                        showRepository.save(translatedShow)
                    }
                }
            }
            return@let filteredShowList
        }
        return if (!seriesListFromOMDB.isNullOrEmpty()){
            seriesListFromOMDB
        } else {
            mutableListOf()
        }
    }


    private fun getDetailsShowFromOmdb(omdbId: String): Show {
        val uri = "https://www.omdbapi.com/?apikey=5b4e4596&i=${omdbId}"
        val restTemplate = RestTemplate()
        val result = restTemplate.getForObject(uri, OmdbShowExtended::class.java)

        return result?.let { omdbShow ->
            return@let Show(0,
                    omdbId = omdbShow.imdbID,
                    showName = omdbShow.title,
                    showCount = 0,
                    showGenre = omdbShow.genre?: "",
                    showType = omdbShow.type,
                    showPosterUrl = omdbShow.poster)
        } ?: Show(0, "")
    }

    private fun scaleDown(posterUrl: String): String{
        val dimenJpg = posterUrl.substringAfterLast("SX")
        val sda = dimenJpg.substringBeforeLast(".")
        val dimension = "100"
        return posterUrl.substringBeforeLast("SX").plus("SX").plus(dimension).plus(".jpg")
    }
}

