package com.wultimaproject.tellmeyours.controls

import com.wultimaproject.tellmeyours.models.Show
import com.wultimaproject.tellmeyours.services.ShowService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class ShowController(val showService: ShowService) {

    @PostMapping("/shows")
    fun voteShow(@RequestBody voteShow: Show): ResponseEntity<Show?> {
        val idToTransmit = voteShow.omdbId
        val platform = voteShow.showStreamingPlatform
        showService.voteShow(idToTransmit, platform)
        return ResponseEntity(voteShow, HttpStatus.CREATED)
    }

    @GetMapping("shows/highestCount")
    fun getShowWithHighestCount(): ResponseEntity<List<Show?>> {
        return ResponseEntity(showService.getShowWithHighestCount(), HttpStatus.OK)
    }

    @GetMapping("shows/genres")
    fun getAllCategories(): ResponseEntity<List<String>> {
        return ResponseEntity(showService.getAllShowGenres(), HttpStatus.OK)
    }

    @GetMapping("/shows")
    fun getShowsWithParameters(
            @RequestParam("showName", required = false) showName: String?,
            @RequestParam("omdbId", required = false) omdbId: String?,
            @RequestParam("showGenre", required = false) showGenre: String?,
            @RequestParam("sortOrder", required = false) sortOrder: String?)
            : ResponseEntity<List<Show?>> {
        val shows = showService.getShowsWithParameters(showName, omdbId, showGenre, sortOrder)
        return ResponseEntity(shows, HttpStatus.OK)
    }

    @GetMapping("/shows/search")
    fun getShowListFromOmdb(
            @RequestParam("showName") showName: String): ResponseEntity<List<Show>> {
        return ResponseEntity(showService.getShowListFromOmdb(showName), HttpStatus.OK)
    }


}